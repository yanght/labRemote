# - Try to find libps6000
# Once done this will define
#  LIBPS6000_FOUND - System has libps6000
#  LIBPS6000_INCLUDE_DIRS - The libps6000 include directories
#  LIBPS6000_LIBRARIES - The libraries needed to use libps6000
#  LIBPS6000_DEFINITIONS - Compiler switches required for using libps6000

FIND_PATH(LIBPS6000_INCLUDE_DIR ps6000Api.h 
          HINTS /usr/include /opt/picoscope/include
          PATH_SUFFIXES libps6000 libps6000-1.4)

FIND_LIBRARY(LIBPS6000_LIBRARY NAMES ps6000 libps6000
              HINTS /usr/lib64 /opt/picoscope/lib )

INCLUDE(FindPackageHandleStandardArgs)
# handle the QUIETLY and REQUIRED arguments and set LIBPS6000_FOUND to TRUE
# if all listed variables are TRUE
FIND_PACKAGE_HANDLE_STANDARD_ARGS(libps6000 DEFAULT_MSG
  LIBPS6000_LIBRARY LIBPS6000_INCLUDE_DIR)

MARK_AS_ADVANCED(LIBPS6000_INCLUDE_DIR LIBPS6000_LIBRARY )

SET(LIBPS6000_LIBRARIES ${LIBPS6000_LIBRARY} )
SET(LIBPS6000_INCLUDE_DIRS ${LIBPS6000_INCLUDE_DIR} )

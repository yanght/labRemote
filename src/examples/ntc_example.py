import labRemote
from labRemote import devcom, com

import time
from argparse import ArgumentParser
import sys

import logging
logging.basicConfig()
logger = logging.getLogger("ntc_example")

# Voltage powering the NTC
NTC_VOLTAGE = 5.0
# Resistance of resistor in voltage divider
R_VOLTAGE_DIVIDER = 10000.0 # ohms
# The total voltage across the voltage divider
VOLTAGE_DIVIDER_REF = 5.0 # volts

# Information specific to the NTC
# Part number: 103KT1608
# See https://www.datasheets360.com/pdf/6686957959228959166
# ---------------------------------------------------------
# Reference temperature in Kelvin
NTC_REF_TEMP = 298.15
# Resistance at reference temperature in ohms
NTC_R_REF = 10000.0 # ohms
# B value for NTC in Kelvin
NTC_BETA = 3435

def ntc_example(device, pin) :

    ##
    ## initialize the communication line
    ##
    try :
        serial = com.TextSerialCom(device, com.SerialCom.BaudRate.Baud9600)
    except :
        logger.error(f"Unable to initialize serial communication with device at port \"{device}\"")
        sys.exit(1)
    if serial is None :
        logger.error(f"Returned communication device is None")
        sys.exit(1)
    serial.setTermination("\r\n")
    serial.setTimeout(5)
    serial.init()

    ##
    ## initialize the NTCSensor object
    ##
    sensor = devcom.NTCSensor(pin, serial, NTC_REF_TEMP, NTC_R_REF, NTC_BETA, R_VOLTAGE_DIVIDER, VOLTAGE_DIVIDER_REF)
    sensor.init()

    ##
    ## perform measurements continuously
    ##
    while True:
        time.sleep(1)
        try:
            sensor.read();
        except RuntimeError as err:
            logger.error(f"Sensor read error: {err}, continuing...")
            continue
        temp = sensor.temperature()
        logger.info(f"Temperature measurement: {temp} C")

if __name__ == "__main__":

    parser = ArgumentParser(description = "Example for how to use the NTCSensor class")
    parser.add_argument("device-port", type = str,
        help = "Port at which the device is connected (e.g. /dev/ttyAMC0)"
    )
    parser.add_argument("pin", type = str,
        help = "Pin on the sensor to sample from"
    )
    parser.add_argument("-d", "--debug", action = "count", default = 0,
        help = "Enable more verbose printout from labRemote"
    )

    args = parser.parse_args()

    for _ in range(args.debug) :
        labRemote.incrDebug()

    ntc_example(args.device_port, args.pin)

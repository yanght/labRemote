#include <math.h>
#include <unistd.h>

#include <iomanip>
#include <iostream>
#include <memory>

#include "FT232H.h"
#include "I2CDevCom.h"
#include "I2CDevComuino.h"
#include "I2CFTDICom.h"
#include "Logger.h"
#include "SHT85.h"

int main(int argc, char* argv[]) {
#ifdef FTDI
    std::shared_ptr<MPSSEChip> ftdi = std::make_shared<FT232H>(
        MPSSEChip::Protocol::I2C, MPSSEChip::Speed::ONE_HUNDRED_KHZ,
        MPSSEChip::Endianness::MSBFirst);
    std::shared_ptr<I2CCom> i2c = std::make_shared<I2CFTDICom>(ftdi, 0x44);
#else
    std::shared_ptr<TextSerialCom> TC(
        new TextSerialCom("/dev/ttyACM0", SerialCom::BaudRate::Baud9600));
    TC->setTermination("\r\n");
    TC->init();

    std::shared_ptr<I2CCom> i2c(new I2CDevComuino(0x44, TC));
#endif  // FTDI

    std::shared_ptr<ClimateSensor> sensor = std::make_shared<SHT85>(i2c);

    for (uint32_t i = 0; i < 10000; i++) {
        sensor->read();
        std::cout << sensor->temperature() << "\t" << sensor->humidity()
                  << std::endl;
    }

    return 0;
}

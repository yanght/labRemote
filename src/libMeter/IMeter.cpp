#include "IMeter.h"

#include "Logger.h"

IMeter::IMeter(const std::string& name, std::vector<std::string> models) {
    m_name = name;
    m_models = models;
}

void IMeter::setCom(std::shared_ptr<ICom> com) {
    if (!com->is_open()) com->init();

    m_com = com;
    if (!ping(0))
        throw std::runtime_error("Failed communication with the multimeter");
}

bool IMeter::ping(unsigned dev) {
    logger(logWARNING) << "ping() not implemented for this multimeter.";
    return false;
}

void IMeter::checkCompatibilityList() {
    // get model connected to the meter
    std::string idn = identify();

    // get list of models
    std::vector<std::string> models = IMeter::getListOfModels();

    if (models.empty()) {
        logger(logINFO) << "No model identifier implemented for this meter. No "
                           "check is performed.";
        return;
    }

    for (const std::string& model : models) {
        if (idn.find(model) != std::string::npos) return;
    }

    logger(logERROR) << "Unknown meter: " << idn;
    throw std::runtime_error("Unknown meter: " + idn);
}

std::vector<std::string> IMeter::getListOfModels() { return m_models; }

double IMeter::measureDCV(unsigned channel) {
    logger(logWARNING) << "measureDCV() not implemented for this multimeter.";
    return 0;
}

double IMeter::measureRES(unsigned channel, bool use4w) {
    logger(logWARNING) << "measureRES() not implemented for this multimeter.";
    return 0;
}

double IMeter::measureCAP(unsigned channel) {
    logger(logWARNING) << "measureCAP() not implemented for this multimeter.";
    return 0;
}

double IMeter::measureDCI(unsigned channel) {
    logger(logWARNING) << "measureDCI() not implemented for this multimeter.";
    return 0;
}

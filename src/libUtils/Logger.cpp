#include "Logger.h"

loglevel_e logIt::loglevel = logINFO;

const std::string logIt::logString[] = {
    "\033[1;31m[ERROR]   : \033[0m", "\033[1;33m[WARNING] : \033[0m",
    "\033[1;32m[INFO]    : \033[0m", "\033[1;34m[DEBUG]   : \033[0m",
    "\033[1;34m[DEBUG1]  : \033[0m", "\033[1;34m[DEBUG2]  : \033[0m",
    "\033[1;34m[DEBUG3]  : \033[0m", "\033[1;34m[DEBUG4]  : \033[0m"};

logIt::logIt(loglevel_e _loglevel) { _buffer << logString[_loglevel]; }

logIt::~logIt() {
    _buffer << std::endl;
    // This is atomic according to the POSIX standard
    // http://www.gnu.org/s/libc/manual/html_node/Streams-and-Threads.html
    std::cerr << _buffer.str();
}

void logIt::incrDebug() {
    switch (loglevel) {
        case logDEBUG:
            loglevel = logDEBUG1;
            break;
        case logDEBUG1:
            loglevel = logDEBUG2;
            break;
        case logDEBUG2:
            loglevel = logDEBUG3;
            break;
        case logDEBUG3:
            loglevel = logDEBUG4;
            break;
        case logDEBUG4:
            loglevel = loglevel;
            break;
        default:
            loglevel = logDEBUG;
            break;
    }
}

#include <string>

namespace utils {
namespace defs {

    const std::string LABREMOTE_SCHEMA_FILENAME = "labremote_config_schema.json";
    const std::string LABREMOTE_BUILD_DIR = "/home/junwen/xrayctrl_xjw/build";
    const std::string LABREMOTE_INSTALL_PREFIX = "/usr/local";

} // namespace defs
} // namespace utils

#include <stdio.h>
#include <string.h>
#include <string>
#include <iostream>
#include "ControllerNSC.h"

using namespace std;

int main(void)
{
    ControllerNSC *ctrl = new ControllerNSC();
    ctrl->connect();
    ctrl->set_home();
    // ctrl->mv_abs(0, 100); //x-axis 
    // ctrl->mv_abs(0, 100); //x-axis 
    ctrl->mv_rel(0, 100); //x-axis 
    ctrl->get_position(); //x-axis 
    // ctrl->mv_abs(1, 100); //y-axis 
    // while (1) {
    //     cout << "Please type command..." << endl;
    //     string input;
    //     cin >> input;
    //     if (input == "") {
    //         cout << "Empty command" << endl;
    //         continue;
    //     }
	// 	ctrl->write(input.c_str());
	// 	ctrl->print();
    // }
    ctrl->disconnect();
}

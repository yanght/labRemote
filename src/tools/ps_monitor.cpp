#include <getopt.h>
#include <signal.h>
#include <stdlib.h>
#include <unistd.h>

#include <iostream>
#include <memory>
#include <string>
#include <thread>

#include "DataSinkConf.h"
#include "EquipConf.h"
#include "IDataSink.h"
#include "Logger.h"
#include "PowerSupplyChannel.h"

//------ SETTINGS
uint32_t tsleep = 10;
std::string configFile;
std::string channelName;
std::string streamName;
//---------------

bool quit = false;
void cleanup(int signum) { quit = true; }

void usage(char *argv[]) {
    std::cerr << "Usage: " << argv[0]
              << " configfile.json datastreamName channelname" << std::endl;
    std::cerr << "" << std::endl;
    std::cerr << "List of options:" << std::endl;
    std::cerr
        << " -t, --time        Milliseconds between measurements (default: "
        << tsleep << ")" << std::endl;
    std::cerr << " -d, --debug       Enable more verbose printout, use "
                 "multiple for increased debug level"
              << std::endl;
    std::cerr << "" << std::endl;
    std::cerr << "" << std::endl;
}

int main(int argc, char *argv[]) {
    if (argc < 1) {
        usage(argv);
        return 1;
    }

    // Parse command-line
    int c;
    while (1) {
        int option_index = 0;
        static struct option long_options[] = {{"time", no_argument, 0, 't'},
                                               {"debug", no_argument, 0, 'd'},
                                               {0, 0, 0, 0}};

        c = getopt_long(argc, argv, "t:d", long_options, &option_index);
        if (c == -1) break;

        switch (c) {
            case 't':
                tsleep = std::atoi(optarg);
                break;
            case 'd':
                logIt::incrDebug();
                break;
            default:
                std::cerr << "Invalid option '" << c << "' supplied. Aborting."
                          << std::endl;
                std::cerr << std::endl;
                usage(argv);
                return 1;
        }
    }

    if (optind > argc - 3) {
        logger(logERROR) << "Missing positional arguments.";
        usage(argv);
        return 1;
    }

    configFile = argv[optind++];
    streamName = argv[optind++];
    channelName = argv[optind++];

    logger(logDEBUG) << "Settings:";
    logger(logDEBUG) << " Config file: " << configFile;
    logger(logDEBUG) << " stream name: " << streamName;
    logger(logDEBUG) << " PS channel: " << channelName;

    // Register interrupt for cleanup
    signal(SIGINT, cleanup);

    // Create sink
    DataSinkConf ds;
    ds.setHardwareConfig(configFile);
    std::shared_ptr<IDataSink> stream = ds.getDataStream(streamName);

    stream->setTag("Channel", channelName);

    //
    // Create hardware database
    EquipConf hw;
    hw.setHardwareConfig(configFile);

    logger(logDEBUG) << "Configuring power-supply.";
    std::shared_ptr<PowerSupplyChannel> PS =
        hw.getPowerSupplyChannel(channelName);

    stream->setTag("Channel", channelName);
    while (!quit) {
        stream->startMeasurement("powersupply",
                                 std::chrono::system_clock::now());
        stream->setField("Voltage", PS->measureVoltage());
        stream->setField("Current", PS->measureCurrent());
        stream->recordPoint();
        stream->endMeasurement();
        std::this_thread::sleep_for(std::chrono::milliseconds(tsleep));
    }

    return 0;
}

#ifndef __Xray_ControllerNSC_H__
#define __Xray_ControllerNSC_H__

#include "ArcusPerformaxDriver.h"

#define DEFAULT_GALIL_POS -2

#include <string>

#include "ControllerBase.h"

using namespace std;
class ControllerNSC : public ControllerBase {
 public:
    ControllerNSC();
    ~ControllerNSC();

    int connect();
    int disconnect();

    int write(const string &cmd);

    int set_speed(int axis, float sp);
    int set_acc(int axis, float acc);

    int mv_abs(int axis, float value);
    int mv_rel(int axis, float value);
    int stop();

    int get_position();
    int get_speed();

    int set_home();
    int set_center(); //not used 

    void wait(int axis);
    void clear(int axis);
    int sleep(int axis);

    enum AXIS { X = 0, Y = 1, ALL = 2 };
    void print() { printf("Out: %s, In: %s\n", out, in); }

 private:
    unsigned char lpDeviceString[PERFORMAX_MAX_DEVICE_STRLEN];
    AR_HANDLE Handle;  // usb handle
    unsigned char out[64];
    unsigned char in[64];
    int convert_mm_to_turns(double value) {
        // value is the relative distance.
        return value * 1000 / 3.125;
    }
    double convert_turns_to_mm(int turns) { return 3.125 / 1000 * turns; }
    double m_position[2] = {0, 0};  // Absolute position in mm
    const string axis_index_to_name[3] = {"X", "Y", ""};
};

#endif

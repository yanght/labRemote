#include "ControllerNSC.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <vector>
#include <chrono>
#include <thread>
#include <iostream>


#include "Helper.h"

ControllerNSC::ControllerNSC() { Handle = NULL; }

ControllerNSC::~ControllerNSC() { disconnect(); }

int ControllerNSC::connect() {
    AR_DWORD num;
    if (!fnPerformaxComGetNumDevices(&num)) {
        printf("error in fnPerformaxComGetNumDevices\n");
        return 0;
    }
    if (num < 1) {
        printf("No motor found\n");
        return 0;
    }

    if (!fnPerformaxComGetProductString(0, lpDeviceString,
                                        PERFORMAX_RETURN_SERIAL_NUMBER) ||
        !fnPerformaxComGetProductString(0, lpDeviceString,
                                        PERFORMAX_RETURN_DESCRIPTION)) {
        printf("error acquiring product string\n");
        return 0;
    }

    printf("device description: %s\n", lpDeviceString);

    // setup the connection
    if (!fnPerformaxComOpen(0, &Handle)) {
        printf("Error opening device\n");
        return 0;
    }

    if (!fnPerformaxComSetTimeouts(5000, 5000)) {
        printf("Error setting timeouts\n");
        return 0;
    }

    if (!fnPerformaxComFlush(Handle)) {
        printf("Error flushing the coms\n");
        return 0;
    }

    // setup the device
    clear(AXIS::X);
    clear(AXIS::Y);
    write("INC");          // set move to absolute mode; move by an absolute amount 
    write("LSPD=100");     // set low speed
    write("HSPD=12000");   // set high speed
    write("ACC=300");      // set acceleration
    write("EO=3");         // enable device
    write("ID");           // read current
    printf("Arcus Product: %s\n", in);
    write("DN");  // read current
    printf("Device Number: %s\n", in);
    printf("XY stage connected -> getting the position -> ");
    get_position();
    usleep(100);
}

int ControllerNSC::disconnect() {
    if (Handle != NULL) {
        stop();  // Stop any ongoing operations
        if (!fnPerformaxComClose(Handle)) {
            printf("Error Closing\n");
            return 0;
        }

        printf("Motor connection has been closed\n");
        Handle = NULL;
    }
    return 1;
}

int ControllerNSC::write(const string& cmd) {
    if (Handle == NULL) {
        printf("Device is not connected\n");
        return 0;
    }
    memset(out, 0, 64);
    memset(in, 0, 64);

    // printf("------ Command: %s\n", cmd.c_str());
    strcpy((char*)out, cmd.c_str());  // set command
    if (!fnPerformaxComSendRecv(Handle, out, 64, 64, in)) {
        printf("Could not send command %s\n", cmd.c_str());
        return 0;
    }
    // printf("++++++ Output: %s\n", in);
    return 1;
}

int ControllerNSC::stop() {
    if (Handle == 0) return 0;
    int status = write("STOP");
    printf("Disconnecting the stage -> getting the position -> ");
    get_position();
    return status;
}

int ControllerNSC::set_speed(int axis, float sp) {
    if (Handle == 0) return 0;
    int steps = convert_mm_to_turns(sp);
    // Set high speed
    string cmd =
        "HSPD" + axis_index_to_name[axis] + "=" + std::to_string(steps);
    write(cmd);
    // Set low speed to be 1/10 of high speed
    cmd = "LSPD" + axis_index_to_name[axis] + "=" +
          std::to_string(max(steps / 10, 1));
    write(cmd);
    return 1;
}

int ControllerNSC::set_acc(int axis, float acc) {
    if (Handle == 0) return 0;
    int steps = convert_mm_to_turns(acc);
    string cmd = "ACC" + axis_index_to_name[axis] + "=" + std::to_string(steps);
    write(cmd);
    return 1;
}

int ControllerNSC::mv_abs(int axis, float value) {
    if (Handle == 0) return 0;
    int steps = convert_mm_to_turns(value - m_position[axis]);
    printf("Moving %s axis by %d steps\n", axis_index_to_name[axis].c_str(), steps);
    string cmd = axis_index_to_name[axis] + std::to_string(steps);
    write(cmd);
    wait(axis);
    get_position();
    return 1;
}

int ControllerNSC::mv_rel(int axis, float value) {
    if (Handle == 0) return 0;
    int steps = convert_mm_to_turns(value);
    printf("Moving %s axis by %d steps\n", axis_index_to_name[axis].c_str(), steps); //INC mode: x1000 move by the amount 
    string cmd = axis_index_to_name[axis] + std::to_string(steps);
    write(cmd);
    wait(axis);
    get_position();
    return 1;
}

int ControllerNSC::get_position() {
    if (Handle == 0) return 0;
    write("PX");
    m_position[AXIS::X] = convert_turns_to_mm(atoi((char*)in));
    write("PY");
    m_position[AXIS::Y] = convert_turns_to_mm(atoi((char*)in));
    printf("Current position: X = %f mm, Y = %f mm\n", m_position[AXIS::X], m_position[AXIS::Y]);
    return 1;
}

int ControllerNSC::set_home() {
    if (Handle == 0) return 0;
    printf("XY stage set home -> getting the position -> ");
    write("LX-");
    wait(X);
    write("LY-");
    wait(Y); 
    write("PX=0");
    write("PY=0");
    get_position();
    return 1;
}

int ControllerNSC::set_center() { return 0; }

void ControllerNSC::wait(int axis){
    string cmd = "MST" + axis_index_to_name[axis];
    unsigned status;
    do {
        write(cmd);
        status = atoi((char*)in);
        // printf("%s %d\n", cmd.c_str(), status);
        usleep(100);
    } while (status & 0x7);
    if ((status & 128) || (status & 256)){
        printf("Error: running into limits; clearing... \n");
        clear(axis); 
    }
}

void ControllerNSC::clear(int axis){
    string cmd = "CLR" + axis_index_to_name[axis];
    write(cmd);
}

int ControllerNSC::sleep(int msec){
    auto now = std::chrono::steady_clock::now();
    std::this_thread::sleep_for(std::chrono::milliseconds(msec));
    auto end = std::chrono::steady_clock::now();
    std::cout<<"sleep: "<<std::chrono::duration_cast<std::chrono::milliseconds>(end - now).count() <<'\n';
    return 0;
}

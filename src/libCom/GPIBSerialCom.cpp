#include "GPIBSerialCom.h"

#include <fcntl.h>
#include <termios.h>
#include <unistd.h>

#include <cerrno>
#include <cstring>
#include <stdexcept>

#include "Logger.h"
#include "ScopeLock.h"

// Register com
#include "ComRegistry.h"
REGISTER_COM(GPIBSerialCom)

GPIBSerialCom::GPIBSerialCom(uint16_t gpib_addr, const std::string& port,
                             SerialCom::BaudRate baud, bool parityBit,
                             bool twoStopBits, bool flowControl,
                             SerialCom::CharSize charsize)
    : TextSerialCom(port, baud, parityBit, twoStopBits, flowControl, charsize),
      m_gpib_addr(gpib_addr) {}

GPIBSerialCom::GPIBSerialCom() : TextSerialCom() {}

GPIBSerialCom::~GPIBSerialCom() {}

void GPIBSerialCom::init() {
    TextSerialCom::init();
    TextSerialCom::send("++auto 0");

    m_good = true;
}

void GPIBSerialCom::setConfiguration(const nlohmann::json& config) {
    for (const auto& kv : config.items()) {
        if (kv.key() == "gpib_addr") {
            m_gpib_addr = kv.value();
        } else if (kv.key() == "read_tmo_ms") {
            m_read_tmo_ms = kv.value();
        }
    }
    TextSerialCom::setConfiguration(config);
}

void GPIBSerialCom::send(const std::string& buf) {
    ScopeLock lock(this);

    TextSerialCom::send("++addr " + std::to_string(m_gpib_addr));
    TextSerialCom::send(buf);
}

void GPIBSerialCom::send(char* buf, size_t length) {
    send(std::string(buf, length));
}

std::string GPIBSerialCom::receive() {
    ScopeLock lock(this);

    if (m_read_tmo_ms > 0)
        TextSerialCom::send("++read_tmo_ms " + std::to_string(m_read_tmo_ms));

    TextSerialCom::send("++read eoi");

    std::string buf = TextSerialCom::receive();
    unlock();

    // rstrip new lines from end
    while (buf.size() > 0 && (buf.back() == '\n' || buf.back() == '\r'))
        buf.pop_back();

    return buf;
}

uint32_t GPIBSerialCom::receive(char* buf, size_t length) {
    ScopeLock lock(this);
    TextSerialCom::send("++read eoi");

    return TextSerialCom::receive(buf, length);
}

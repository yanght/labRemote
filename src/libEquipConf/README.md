# Equipment configuration
This library provides support for dynamic usage of hardware of the same type without the need of recompiling the program when a different model of the same hardware is used.

The configurations are stored in JSON files and examples can be found in src/configs.

# DataSink configuration
This library provides support for dynamic usage of data measurement types (data sinks) without the need of recompiling the program when a different data format is used.


The configurations are stored in JSON files and examples can be found in src/configs.


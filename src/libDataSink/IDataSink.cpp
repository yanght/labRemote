#include "IDataSink.h"

IDataSink::IDataSink(const std::string& name) : m_name(name) {}

bool IDataSink::checkReserved(const std::string& name) {
    return m_reserved.count(name) > 0;
}

#ifndef DT5471NPS_H
#define DT5471NPS_H

#include "DT54xxPs.h"

//! \brief Implementation for the CAEN DT5471N power supplies
/**
 * [Reference](https://www.caen.it/products/dt5471/)
 *
 * Note: This is the negative output voltage variation.
 */
class DT5471NPs : public DT54xxPs {
 public:
    DT5471NPs(const std::string& name);
    ~DT5471NPs() = default;
};

#endif  // DT5471NPS_H

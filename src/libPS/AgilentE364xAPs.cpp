#include "AgilentE364xAPs.h"

// Register power supply
#include "PowerSupplyRegistry.h"
REGISTER_POWERSUPPLY(AgilentE364xAPs)

AgilentE364xAPs::AgilentE364xAPs(const std::string& name)
    : AgilentPs(name, {"E3642A", "E3644A"}, 1) {}

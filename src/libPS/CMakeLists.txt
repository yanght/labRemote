add_library(PS SHARED)
target_sources(PS
  PRIVATE
  IPowerSupply.cpp
  PowerSupplyChannel.cpp
  PowerSupplyRegistry.cpp

  SCPIPs.cpp
  AgilentPs.cpp
  AgilentE364xAPs.cpp
  AgilentE3631APs.cpp
  AgilentE3634APs.cpp
  AgilentE3648APs.cpp
  AgilentE36300APs.cpp
  DT54xxPs.cpp
  DT5471NPs.cpp
  DT5472NPs.cpp
  SorensenPs.cpp
  RigolDP832.cpp
  Keithley24XX.cpp
  Keithley22XX.cpp
  RS_HMP4040.cpp
  RS_HMP2020.cpp
  TTIPs.cpp
  TTIXXXTPPs.cpp
  TTIXXXDPPs.cpp
  TTIXXXSPPs.cpp
  Bk16XXPs.cpp

  )

target_link_libraries(PS PRIVATE Com Utils ${JSON_LIBS})
target_include_directories(PS PUBLIC ${CMAKE_CURRENT_SOURCE_DIR})

set_target_properties(PS PROPERTIES VERSION ${labRemote_VERSION_MAJOR}.${labRemote_VERSION_MINOR})
install(TARGETS PS)

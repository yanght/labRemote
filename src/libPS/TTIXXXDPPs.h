#ifndef TTIXXXDPPS_H
#define TTIXXXDPPS_H

#include <chrono>
#include <memory>
#include <string>

#include "TTIPs.h"

/** \brief TTi XXXDP
 *
 * Implementation for the TTi dual channel power supplies
 * such as PL330DP.
 *
 */
class TTIXXXDPPs : public TTIPs {
 public:
    TTIXXXDPPs(const std::string& name);
    ~TTIXXXDPPs() = default;
};

#endif

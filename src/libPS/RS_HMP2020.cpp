#include "RS_HMP2020.h"

#include <algorithm>
#include <thread>

#include "Logger.h"

// Register power supply
#include "PowerSupplyRegistry.h"
REGISTER_POWERSUPPLY(RS_HMP2020)

RS_HMP2020::RS_HMP2020(const std::string& name)
    : SCPIPs(name, {"HMP2020"}, 2) {}

#ifndef TTIXXXTPPS_H
#define TTIXXXTPPS_H

#include "TTIPs.h"

/** \brief TTi MX180TPP
 *
 * Implementation for the TTi triple channel power supplies
 * such as MX180TP, QL355TP
 *
 * [Progamming
 * Manual](http://resources.aimtti.com/manuals/MX180T+MX180TP_Instruction_Manual-Iss5.pdf)
 *
 * Similar to other TTi power supplies, so based on generic TTi class
 */
class TTIXXXTPPs : public TTIPs {
 public:
    TTIXXXTPPs(const std::string& name);
    ~TTIXXXTPPs() = default;
};

#endif

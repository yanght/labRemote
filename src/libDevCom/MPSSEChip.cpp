
// labremote
#include "MPSSEChip.h"

#include "ComIOException.h"
#include "Logger.h"

// libmpsse
extern "C" {
#include "mpsse.h"
}

// std/stl
#include <sstream>
#include <stdexcept>
#include <tuple>  // std::get<>

MPSSEChip::MPSSEChip(MPSSEChip::Protocol protocol, MPSSEChip::Speed speed,
                     MPSSEChip::Endianness endianness, int vendor_id,
                     int product_id, const std::string& description,
                     const std::string& serial) {
    logger(logDEBUG)
        << "Initializing MPSSEChip with (protocol, speed, endianness)=("
        << protocol2str(protocol) << ", " << speed2str(speed) << ", "
        << endianness2str(endianness) << ")";

    if (protocol != MPSSEChip::Protocol::I2C) {
        std::stringstream warning;
        warning << "You have selected a serial protocol (\""
                << MPSSEChip::protocol2str(protocol) << "\")"
                << " that has not been tested -- caveat emptor!";
        logger(logWARNING) << warning.str();
    }

    // get the libmpsse instances of the enumerated items
    enum modes mode;
    switch (protocol) {
        case MPSSEChip::Protocol::I2C: {
            mode = I2C;
            break;
        }
        case MPSSEChip::Protocol::SPI0: {
            mode = SPI0;
            break;
        }
        case MPSSEChip::Protocol::SPI1: {
            mode = SPI1;
            break;
        }
        case MPSSEChip::Protocol::SPI2: {
            mode = SPI2;
            break;
        }
    }  // switch

    int end = (endianness == MPSSEChip::Endianness::MSBFirst) ? MSB : LSB;

    if (vendor_id >= 0 && product_id >= 0) {
        if (!is_known_ftdi_device(vendor_id, product_id)) {
            std::stringstream warning;
            warning
                << "You have selected a device (VID=0x" << std::hex << vendor_id
                << ", PID=0x" << std::hex << product_id << std::dec << ")"
                << " that has not been tested to be a valid FTDI/MPSSE device";
            logger(logWARNING) << warning.str();
        }
        logger(logDEBUG) << "Opening FTDI device at 0x" << std::hex << vendor_id
                         << ":0x" << product_id;

        // opens the specific USB device with VID = vendor_id and PID =
        // product_id and serial number = serial NOTE: we specify IFACE_A since
        // on FT232H there is only one interface,
        //       but on other FTDI MPPSE may have several
        // Open(...) is defined in libmpsse
        if ((m_mpsse = Open(
                 vendor_id, product_id, mode, static_cast<int>(speed), end,
                 IFACE_A, (!description.empty()) ? description.c_str() : NULL,
                 (!serial.empty()) ? serial.c_str() : NULL)) == NULL ||
            !m_mpsse->open)
            throw ComIOException("Failed to open MPSSE device");
    } else {
        logger(logDEBUG) << "Opening FTDI device";

        // opens the first FTDI device found
        // MPSSE(...) is defined in libmpsse
        if ((m_mpsse = MPSSE(mode, static_cast<int>(speed), end)) == NULL ||
            !m_mpsse->open)
            throw ComIOException("Failed to open MPSSE device");
    }

    if (protocol == MPSSEChip::Protocol::I2C) {
        Tristate(m_mpsse);
    }

    m_protocol = protocol;
    m_speed = speed;
    m_endianness = endianness;
}

MPSSEChip::~MPSSEChip() {
    // Close is defined in libmpsse
    Close(m_mpsse);
}

bool MPSSEChip::start() {
    // Start is defined in libmpsse
    return Start(m_mpsse) == MPSSE_OK;
}

bool MPSSEChip::stop() {
    // Stop is defined in libmpsse
    return Stop(m_mpsse) == MPSSE_OK;
}

bool MPSSEChip::write(char* data, int size) {
    // Write is defined in libmpsse
    return Write(m_mpsse, data, size) == MPSSE_OK;
}

void MPSSEChip::gpio_write(int pin, int state) {
    // PinLow and PinHigh are defined in libmpsse
    if (state == 0)
        PinLow(mpsse_ctx(), pin);
    else
        PinHigh(mpsse_ctx(), pin);
}

bool MPSSEChip::get_ack() {
    // GetAck is defined in libmpsse
    return GetAck(mpsse_ctx()) == ACK;
}

void MPSSEChip::set_ack() {
    // SetAck is defined in libmpsse
    SetAck(mpsse_ctx(), ACK);
}

void MPSSEChip::set_nack() {
    // SetAck is defined in libmpsse
    SetAck(mpsse_ctx(), NACK);
}

char* MPSSEChip::read(int size) {
    // Read is defined in libmpsse
    return Read(mpsse_ctx(), size);
}

std::string MPSSEChip::protocol2str(const MPSSEChip::Protocol& proto) {
    std::stringstream os;
    switch (proto) {
        case MPSSEChip::Protocol::I2C: {
            os << "I2C";
            break;
        }
        case MPSSEChip::Protocol::SPI0: {
            os << "SPI0";
            break;
        }
        case MPSSEChip::Protocol::SPI1: {
            os << "SPI1";
            break;
        }
        case MPSSEChip::Protocol::SPI2: {
            os << "SPI2";
            break;
        }
        case MPSSEChip::Protocol::SPI3: {
            os << "SPI3";
            break;
        }
        default: {
            os << "MPSSEChip::ProtocolInvalid";
            break;
        }
    }  // switch
    return os.str();
}

std::string MPSSEChip::endianness2str(const MPSSEChip::Endianness& endianness) {
    std::stringstream os;
    switch (endianness) {
        case MPSSEChip::Endianness::MSBFirst: {
            os << "MSBFirst";
            break;
        }
        case MPSSEChip::Endianness::LSBFirst: {
            os << "LSBFirst";
            break;
        }
        default: {
            os << "MPSSEChip::EndiannessInvalid";
            break;
        }
    }  // switch
    return os.str();
}

std::string MPSSEChip::speed2str(const MPSSEChip::Speed& speed) {
    std::stringstream os;
    switch (speed) {
        case MPSSEChip::Speed::ONE_HUNDRED_KHZ:
        case MPSSEChip::Speed::FOUR_HUNDRED_KHZ:
        case MPSSEChip::Speed::ONE_MHZ:
        case MPSSEChip::Speed::TWO_MHZ:
        case MPSSEChip::Speed::FIVE_MHZ:
        case MPSSEChip::Speed::SIX_MHZ:
        case MPSSEChip::Speed::TEN_MHZ:
        case MPSSEChip::Speed::TWELVE_MHZ:
        case MPSSEChip::Speed::FIFTEEN_MHZ:
        case MPSSEChip::Speed::THIRTY_MHZ:
        case MPSSEChip::Speed::SIXTY_MHZ: {
            os << static_cast<int>(speed);
            break;
        }
        default: {
            os << "MPSSEChip::SpeedInvalid";
            break;
        }
    }  // switch
    return os.str();
}

std::string MPSSEChip::to_string() {
    std::stringstream sx;
    sx << "Protocol: " << MPSSEChip::protocol2str(this->protocol())
       << ", Speed: " << MPSSEChip::speed2str(this->speed())
       << ", Endianness: " << MPSSEChip::endianness2str(this->endianness());
    return sx.str();
}

bool MPSSEChip::is_known_ftdi_device(int vid, int pid) {
    for (std::pair<int, int> device : m_known_ftdi_devices) {
        int ivid = std::get<0>(device);
        int ipid = std::get<1>(device);
        if (ivid == vid && ipid == pid) return true;
    }
    return false;
}

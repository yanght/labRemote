#include "PtSensor.h"

#include "DeviceComRegistry.h"
REGISTER_DEVCOM(PtSensor, ClimateSensor)

#include <cmath>

#include "NotSupportedException.h"

PtSensor::PtSensor(uint8_t chan, std::shared_ptr<ADCDevice> dev, float Rref,
                   float Apt, float Bpt, float Rdiv)
    : m_adcdev(dev),
      m_chan(chan),
      m_Rref(Rref),
      m_Apt(Apt),
      m_Bpt(Bpt),
      m_Rdiv(Rdiv) {}

PtSensor::~PtSensor() {}

void PtSensor::init() {}

void PtSensor::reset() {}

void PtSensor::read() {
    m_temperature = 0.;
    for (uint i = 0; i < 10; i++)
        m_temperature += (float)m_adcdev->read(m_chan);
    m_temperature /= 10;
    m_temperature = raw_to_C(m_temperature);
}

float PtSensor::temperature() const { return m_temperature; }

float PtSensor::humidity() const {
    throw NotSupportedException("Humidity not supported for Pt");
    return 0;
}

float PtSensor::pressure() const {
    throw NotSupportedException("Pressure not supported for Pt");
    return 0;
}

float PtSensor::raw_to_C(float raw) {
    // If there is a reading error, return a default value
    if (raw >= 1.) return -273.15;
    float res =
        m_Apt * m_Apt - 4. * m_Bpt * (1. - m_Rdiv / m_Rref * (1. / raw - 1.));
    if (res < 0.)
        return -273.15;
    else
        return (std::sqrt(res) - m_Apt) / 2. / m_Bpt;
}

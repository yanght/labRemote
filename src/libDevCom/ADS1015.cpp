#include "ADS1015.h"

// std/stl
#include <algorithm>  // std::find, std::find_if
#include <chrono>
#include <iomanip>
#include <sstream>
#include <string>
#include <thread>  // std::this_thread

// labRemote
#include "DeviceComRegistry.h"
#include "I2CCom.h"
#include "LinearCalibration.h"
#include "Logger.h"
REGISTER_DEVCOM(ADS1015, ADCDevice)

const std::map<ADS1015::FullScaleRange, double> ADS1015::fsrToValMap = {
    {ADS1015::FullScaleRange::FSR_6_144, 6.144},
    {ADS1015::FullScaleRange::FSR_4_096, 4.096},
    {ADS1015::FullScaleRange::FSR_2_048, 2.048},
    {ADS1015::FullScaleRange::FSR_1_024, 1.024},
    {ADS1015::FullScaleRange::FSR_0_512, 0.512},
    {ADS1015::FullScaleRange::FSR_0_256, 0.256}};

const std::map<ADS1015::Gain, ADS1015::FullScaleRange> ADS1015::gainToFSRMap = {
    {ADS1015::Gain::xTWOTHIRDS, ADS1015::FullScaleRange::FSR_6_144},
    {ADS1015::Gain::xONE, ADS1015::FullScaleRange::FSR_4_096},
    {ADS1015::Gain::xTWO, ADS1015::FullScaleRange::FSR_2_048},  // default
    {ADS1015::Gain::xFOUR, ADS1015::FullScaleRange::FSR_1_024},
    {ADS1015::Gain::xEIGHT, ADS1015::FullScaleRange::FSR_0_512},
    {ADS1015::Gain::xSIXTEEN, ADS1015::FullScaleRange::FSR_0_256}};

ADS1015::ADS1015(std::shared_ptr<I2CCom> com)
    : ADCDevice(std::make_shared<LinearCalibration>(
          fsrToValMap.at(FullScaleRange::FSR_2_048), m_maxValue)) {
    m_com = com;
}

ADS1015& ADS1015::setGain(Gain gain) {
    return setFullScaleRange(gainToFSRMap.at(gain));
}

ADS1015& ADS1015::setFullScaleRange(FullScaleRange range) {
    double full_scale_range = fsrToValMap.at(range);
    m_fullscale_range = range;
    this->setCalibration(
        std::make_shared<LinearCalibration>(full_scale_range, m_maxValue));
    return *this;
}

ADS1015& ADS1015::setChannelMode(ChannelMode mode) {
    m_channelMode = mode;
    return *this;
}

int32_t ADS1015::readCount() {
    // set channel to default channel
    return readCount(0);
}

int32_t ADS1015::readCount(uint8_t channel) {
    validateChannel(channel);

    uint16_t config = 0;
    configSingleShot(config);

    // set the programmable gain amplifier configuration
    configPGA(config);

    // set the channel to sample from
    configChannel(config, channel);

    // set 'start single-conversion' bit
    configStartConversion(config);

    // write the configuration
    m_com->write_reg16(static_cast<uint16_t>(Address::POINTER_CONFIG), config);
    while (!conversionComplete()) {
    }

    uint16_t result =
        m_com->read_reg16(static_cast<uint16_t>(Address::POINTER_CONVERT));

    // ADS1015 writes 12-bit results shifted by four, so remove the shift
    result = result >> 4;

    if (m_channelMode == ChannelMode::Differential) {
        // differential measurements are signed, so check the sign bit
        if (result > 0x07FF) {
            // negative number: set the sign bit
            result |= 0xF000;
        }
    }
    return static_cast<int32_t>(result);
}

void ADS1015::readCount(const std::vector<uint8_t>& channels,
                        std::vector<int32_t>& counts) {
    counts.clear();
    for (const uint8_t channel : channels) {
        counts.push_back(readCount(channel));
    }
}

void ADS1015::configChannel(uint16_t& config, uint8_t channel) {
    switch (m_channelMode) {
        case ChannelMode::SingleEnded:
            configSingleEndedChannel(config, channel);
            break;
        case ChannelMode::Differential:
            configDifferentialChannel(config, channel);
            break;
        default:
            throw std::out_of_range(
                "Invalid measurement mode, cannot configure ADS1015 channel");
            break;
    }  // swtich
}

void ADS1015::configSingleEndedChannel(uint16_t& config, uint8_t channel) {
    switch (channel) {
        case 0:
            config |= static_cast<uint16_t>(Config::CONFIG_MUX_SINGLE_0);
            break;
        case 1:
            config |= static_cast<uint16_t>(Config::CONFIG_MUX_SINGLE_1);
            break;
        case 2:
            config |= static_cast<uint16_t>(Config::CONFIG_MUX_SINGLE_2);
            break;
        case 3:
            config |= static_cast<uint16_t>(Config::CONFIG_MUX_SINGLE_3);
            break;
        default:
            std::stringstream e;
            e << "Invalid channel provided for ADS1015 device (channel given = "
              << channel << ")";
            throw std::out_of_range(e.str());
            break;
    }
}

void ADS1015::configDifferentialChannel(uint16_t& config, uint8_t channel) {
    switch (channel) {
        case 0:
            config |= static_cast<uint16_t>(Config::CONFIG_MUX_DIFF_P0_N1);
            break;
        case 1:
            config |= static_cast<uint16_t>(Config::CONFIG_MUX_DIFF_P2_N3);
            break;
        default:
            throw std::runtime_error(
                "Invalid differential channel config provided for ADS1015 "
                "device");
            break;
    }
}

void ADS1015::configSingleShot(uint16_t& config) {
    config |=
        static_cast<uint16_t>(
            Config::CONFIG_CQUE_NONE) |  // Disable the comparator (default val)
        static_cast<uint16_t>(
            Config::CONFIG_CLAT_NONLAT) |  // Non-latching (default val)
        static_cast<uint16_t>(
            Config::CONFIG_CPOL_ACTVLOW) |  // Alert/Rdy active low   (default
                                            // val)
        static_cast<uint16_t>(
            Config::CONFIG_CMODE_TRAD) |  // Traditional comparator (default
                                          // val)
        static_cast<uint16_t>(
            Config::CONFIG_RATE_1600HZ) |  // 1600 samples per second (default)
        static_cast<uint16_t>(
            Config::CONFIG_MODE_SINGLE);  // Single-shot mode (default)
}

void ADS1015::configPGA(uint16_t& config) {
    // configure programmable gain amplifier based on the configured voltage
    // reference
    Gain gain = gainFromFSR(m_fullscale_range);
    config |= static_cast<uint16_t>(gainToConfigMap.at(gain));
}

void ADS1015::configStartConversion(uint16_t& config) {
    // set 'start single-conversion' bit
    config |= static_cast<uint16_t>(Config::CONFIG_OS_SINGLE);
}

bool ADS1015::conversionComplete() {
    uint16_t reg =
        m_com->read_reg16(static_cast<uint16_t>(Address::POINTER_CONFIG));
    return (reg & 0x8000) != 0;
}

void ADS1015::validateChannel(uint8_t channel) {
    uint8_t max_channels;
    std::string mode;
    switch (m_channelMode) {
        case ChannelMode::SingleEnded:
            max_channels = 4;
            mode = "SingleEnded";
            break;
        case ChannelMode::Differential:
            max_channels = 2;
            mode = "Differential";
            break;
    };
    if (channel >= max_channels) {
        std::stringstream e;
        e << "Invalid channel (=" << static_cast<unsigned>(channel)
          << ") for ADS1015 channel mode (=" << mode << ") having "
          << static_cast<unsigned>(max_channels) << " maximum allowed channels";
        throw std::out_of_range(e.str());
    }
}

ADS1015::Gain ADS1015::gainFromFSR(FullScaleRange range) const {
    auto it =
        std::find_if(gainToFSRMap.begin(), gainToFSRMap.end(),
                     [&range](const std::pair<Gain, FullScaleRange>& item) {
                         return item.second == range;
                     });

    // shouldn't happen
    if (it == gainToFSRMap.end()) {
        throw std::runtime_error("Invalid FullScaleRange value");
    }
    return it->first;
}

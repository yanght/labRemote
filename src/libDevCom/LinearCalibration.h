#ifndef LINEARCALIBRATION_H
#define LINEARCALIBRATION_H

#include "DeviceCalibration.h"

//! \brief Calibration that assumes a linear relationship between counts and the
//! output value
/**
 * The conversion uses the following formula to convert counts (signed integer)
 * to a value:
 *
 * ```
 *  value = reference * counts / max + offset
 * ```
 *
 * For example, an 8 bit ADC with a 3.3V reference would use
 * - `reference = 3.3`
 * - `max = 0xFF`
 * - `offset = 0.0`
 *
 * The counts are assumed to represented by a signed integer. This allows
 * support for differential devices with negative values represented as such.
 */
class LinearCalibration : public DeviceCalibration {
 public:
    /**
     * \param reference Voltage at `max` counts
     * \param max Counts at `reference` value
     * \param offset Linear offset.
     */
    LinearCalibration(double reference, uint32_t max, double offset = 0);

    virtual ~LinearCalibration() = default;

    virtual double calibrate(int32_t counts);
    virtual int32_t uncalibrate(double value);

 private:
    double m_reference;
    int32_t m_max;
    double m_offset = 0;
};

#endif  // LINEARCALIBRATION_H

#ifndef FT232H_H
#define FT232H_H

// these map the GPIO functionality of the ADBUS and ACBUS to the pin numbering
// scheme used by libmpsse (excludes default SK, D0, DI, CS pins)
#define GPIO_ADBUS_4 0
#define GPIO_ADBUS_5 1
#define GPIO_ADBUS_6 2
#define GPIO_ADBUS_7 3
#define GPIO_ACBUS_0 4
#define GPIO_ACBUS_1 5
#define GPIO_ACBUS_2 6
#define GPIO_ACBUS_3 7
#define GPIO_ACBUS_4 8
#define GPIO_ACBUS_5 9
#define GPIO_ACBUS_6 10
#define GPIO_ACBUS_7 11

#include "MPSSEChip.h"

//! \brief MPSSE control of the FT232H chip
/**
 * [Specificatons](https://www.ftdichip.com/Support/Documents/DataSheets/ICs/DS_FT232H.pdf)
 *
 * Vendor ID: 0x0403
 * Product ID: 0x6014
 */
class FT232H : public MPSSEChip {
 public:
    FT232H(MPSSEChip::Protocol protocol, MPSSEChip::Speed speed,
           MPSSEChip::Endianness endianness,
           const std::string& description = "", const std::string& serial = "");

 private:
};  // FT232H

#endif  // FT232H_H

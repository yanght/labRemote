#include "IChiller.h"

IChiller::IChiller() {}

IChiller::~IChiller() {}

void IChiller::setCom(std::shared_ptr<ICom> com) {
    if (!com->is_open()) com->init();
    m_com = com;
}

float IChiller::ctof(float t) { return (t * 9.0 / 5.0) + 32.0; }

float IChiller::ftoc(float t) { return (t - 32.0) * 5.0 / 9.0; }

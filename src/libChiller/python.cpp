#include <pybind11/pybind11.h>
#include <pybind11/stl.h>

#include "IChiller.h"
#include "PolySciLM.h"

namespace py = pybind11;

class PyIChiller : public IChiller {
 public:
    using IChiller::IChiller;

    void init() override { PYBIND11_OVERLOAD_PURE(void, IChiller, init, ); }

    void turnOn() override { PYBIND11_OVERLOAD_PURE(void, IChiller, turnOn, ); }

    void turnOff() override {
        PYBIND11_OVERLOAD_PURE(void, IChiller, turnOff, );
    }

    void setTargetTemperature(float temp) override {
        PYBIND11_OVERLOAD_PURE(void, IChiller, setTargetTemperature, temp);
    }

    float getTargetTemperature() override {
        PYBIND11_OVERLOAD_PURE(float, IChiller, getTargetTemperature, );
    }

    float measureTemperature() override {
        PYBIND11_OVERLOAD_PURE(float, IChiller, measureTemperature, );
    }

    bool getStatus() override {
        PYBIND11_OVERLOAD_PURE(bool, IChiller, getStatus, );
    }
};

void register_chiller(py::module& m) {
    py::class_<IChiller, PyIChiller, std::shared_ptr<IChiller>>(m, "IChiller")
        .def(py::init<>())
        .def("init", &IChiller::init)
        .def("setCom", &IChiller::setCom)
        .def("turnOn", &IChiller::turnOn)
        .def("turnOff", &IChiller::turnOff)
        .def("setTargetTemperature", &IChiller::setTargetTemperature)
        .def("getTargetTemperature", &IChiller::getTargetTemperature)
        .def("measureTemperature", &IChiller::measureTemperature)
        .def("getStatus", &IChiller::getStatus);

    py::class_<PolySciLM, IChiller, std::shared_ptr<PolySciLM>>(m, "PolySciLM")
        .def(py::init<>())
        .def("init", &PolySciLM::init)
        .def("turnOn", &PolySciLM::turnOn)
        .def("turnOff", &PolySciLM::turnOff)
        .def("setTargetTemperature", &PolySciLM::setTargetTemperature)
        .def("getTargetTemperature", &PolySciLM::getTargetTemperature)
        .def("measureTemperature", &PolySciLM::measureTemperature)
        .def("getStatus", &PolySciLM::getStatus)
        .def("getFaultStatus", &PolySciLM::getFaultStatus);
}
